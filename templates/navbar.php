
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">PHP E-COMMERCE</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor02">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
      <a class="nav-link" href="catalog.php">Catalog <span class="sr-only">(current)</span></a>
    </li>
      <?php
        require "../controllers/connection.php";
        session_start();
          if(isset($_SESSION['user'])){
            if ($_SESSION['user']['role_id']== 1){
                ?>
                <li class="nav-item">
                  <a class="nav-link" href="../views/add_item_form.php">Add Item</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="../views/admin_orders.php">All Orders</a>
                </li>
                <?php
            }else{?>
              <li class="nav-item">
                <a class="nav-link" href="../views/cart.php">Show Cart <span class="badge bg-dark text-light" id="cartCount">
                <?php
                if (isset($_SESSION["cart"])) {
                  echo array_sum($_SESSION["cart"]);
                } else {
                  echo 0;
                }
                ?>
              </span></a>
              </li>
          <?php
            }
            ?>
          <li class="nav-item">
            <a class="nav-link" href="#">Hello, Its Me <?php echo $_SESSION['user']['firstName'] ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../controllers/process_logout.php">logout</a>
          </li>
          <?php
        }else{
          ?>
            <li class="nav-item">
              <a class="nav-link" href="../views/login.php">login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../views/register.php">Register</a>
            </li>
            <?php
        }
      ?>
  </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>




