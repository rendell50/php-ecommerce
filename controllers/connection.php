<?php 

	$host = "localhost"; //the host to use
	$db_username = "root"; // the username for the host
	$db_password = ""; //password for the host
	$db_name = "b46Ecommerce"; //database that we will use

	// create a connection 

	$conn = mysqli_connect($host, $db_username, $db_password, $db_name);
	
	//check if connection is successful
	if(!$conn){
		die("Connection failed:" .mysqli_error($conn));


	}
 ?>