<?php 
	//we'll save the value of $sort in a session variable so we can use it in our view.
	session_start();
	if(isset($_GET['sort'])){
		if($_GET['sort'] == "asc"){
			$_SESSION['sort'] = " ORDER BY price ASC";

		}else{
			$_SESSION['sort'] = " ORDER BY price DESC";
		}
	}
	header("LOCATION: ". $_SERVER['HTTP_REFERER']);
 ?>