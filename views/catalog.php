<?php 
	require "../templates/template.php";
	function get_content(){

		//We need to require the connection file everytime we need something from thwe database
		require "../controllers/connection.php";
 ?>

 	<h1 class="text-center py-5">CATAGALOG PAHINA!</h1>
 	<div class="container">
 	<div class="row">
 		<div class="col-lg-2">
 			<!-- sidebar -->
 			<h6>Categories</h6>
 			<ul class="list-group-border">
 				<li class="list-group-item">
 					<a href="catalog.php">All</a>
 				</li>
 				<?php 

 				//review this later

 				//CALL CATEGORIES
 				$categories_query = "SELECT * FROM categories";
 				$categoryList = mysqli_query($conn, $categories_query);
 				foreach ($categoryList as $indiv_category){
 				 ?>
 				 <li class="list-group-item">
 				 	<a href="catalog.php?category_id=<?php echo$indiv_category['id']?>"><?php echo $indiv_category['name']?></a>
 				 </li>
 				<?php 
 				}
 				 ?>
 			</ul>
 			<!-- sorting -->
 			<h6>Sort By Price:</h6>
 			<ul class="list-group-border">
 				<li class="list-group-item">
 					<a href="../controllers/process_sort.php?sort=asc">(Lowest to Highest)</a>
 					<a href="../controllers/process_sort.php?sort=desc">(Highest to Lowest)</a>
 				</li>
 				
 			</ul>
 			
 		</div>
 		<!-- Item list -->
 		<div class="col-lg-10">
 			<div class="row">
 			<?php 
 			
 		//mysqli ($connection variable from connection.php, query)

 		$items_query = "SELECT * FROM items";
 			// for filtering 
 		if(isset($_GET['category_id'])){
 			//concatenate . = " space "
 			$catId = $_GET['category_id'];
 			$items_query .= " WHERE category_id = $catId";
 		}
			//sort
			if(isset($_SESSION['sort'])){
			$items_query .= $_SESSION['sort'];
			}
			

 		$items = mysqli_query($conn, $items_query);
 		foreach ($items as $indiv_item) {
 			?>
 			<div class="col-lg-4 py-2">
 				<div class="card">
 					<img class="card-img-top" height="300px" src="<?php echo $indiv_item ['image']?>" alt="image">
 					<div class="card-body">
 						<h4 class="card-title"><?php echo $indiv_item['name']?></h4>
 						<p class="card-text">Php<?php echo $indiv_item['price']?>.00</p>
 						<p class="card-text"><?php echo $indiv_item['description']?></p>
 						<?php 
 						$catId = $indiv_item['category_id'];

 						$category_query = "SELECT * FROM categories WHERE id = $catId";
 						$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
 						
 						?>
 						<p class="card-text">Category: <?php echo $category['name']?></p>
 						
 						<!-- 
 							// process of displaying category
 						//1. get the category name where id equal to $indiv_item['category_id']
 						//2 display the data  -->
 						
 					</div>
 					<?php 
 						if(isset($_SESSION['user']) && $_SESSION['user']['role_id']==1){
 							?>
 					<div class="card-footer">
 						<a href="edit_item_form.php?id=<?php echo $indiv_item['id']?>" class="btn btn-warning">Edit Item</a>
 						<a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id']?>" class="btn btn-danger">Delete Item</a>
 					</div>
 					<?php 
 						}else{
 							?>
 					<div class="card-footer">
 							<!-- <form action="../controllers/process_update_cart.php" method="POST">
 								<input class="form-control" type="hidden" value="1" name="">
 								<button class="btn btn-primary" type="submit">Add to Cart</button>
 							</form> -->

 							<input type="number" name="quantity" class="form-control" value="1">
 							<button class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id'] ?>">Add To Cart</button>
 							
 					</div>
 					<?php 
 						};
 					 ?>

 					
 				</div>
 			</div>
 			<?php 
 		}

 		//Steps for retrieving items
 		//1. Create a query
 		//2. Use mysqli_query to get the results
 		//3. if array (use foreach)
 		//4. if object (use mysqli_fetch_assoc to an associative array)
 		//4.1 Use foreach ($result as $key => $value)
 		
 		?>
 		</div>
 			
 		</div>
 		
 	</div>
 </div>

 <script type="text/javascript" src="../assets/scripts/addtocart.js"></script>
 

 	<?php 
 }

 	 ?>




