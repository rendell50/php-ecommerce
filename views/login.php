<?php 

	require "../templates/template.php";
	function get_content(){


 ?>
 <h1 class="text-center py-5">LOGIN</h1>
 <div class="col-lg-4 offset-lg-4">
 	<form action="" method="POST">
 		<div class="form-group">
 			<label for="email">Email:</label>
 			<input type="email" name="email" class="form-control" id="email">
 			<span></span>
 		</div>
 		
 		<div class="form-group">
 			<label for="password">Password:</label>
 			<input type="password" name="password" class="form-control" id="password">
 		</div>
 		
 	</form>
 	<button class="btn btn-info" type="button" id="loginUser">Login</button>
 	<p>Not yet registered? <a href="register.php">Register</a></p>
 </div>

 <script src="../assets/scripts/login.js"></script>
 <?php  
}
 ?>
