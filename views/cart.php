<?php 
require"../templates/template.php";

function get_content(){
	require "../controllers/connection.php";

 ?> 
 <h1 class="text-center py-5">CART</h1>
 <hr>
 <div class="table-responsive">
 	
 		
 			<table class="table table-striped">
 				<thead>
 					<tr class="text-center">
 					<th>Item:</th>
 					<th>Price:</th>
 					<th>Quantity:</th>
 					<th>Sub Total:</th>
 					</tr>
 				</thead>
 				<tbody>
 					<?php 
 					$total=0;
 					//1. To check whether we have $_SESSION CART
 					if (isset($_SESSION['cart'])){
 						foreach($_SESSION['cart']as $item_id => $item_quantity){
 							$item_query= "SELECT * FROM items WHERE id = $item_id";
 							$indiv_item= mysqli_fetch_assoc(mysqli_query($conn, $item_query));
 							$subtotal = $indiv_item['price'] * $item_quantity;
 							$total +=$subtotal;
 							?>
 							<tr>
 								<td><?php echo $indiv_item['name']?></td>
 								<td><?php echo $indiv_item['price']?></td>
 								<td>
 									<form action="../controllers/process_update_cart.php" method="POST">
 										<!-- we need to send the id so we know which item in cart to edti -->
 										<input type="hidden" name="fromCartPage" value="true">
 										<input type="hidden" name="id" value="<?php echo $item_id?>">
 										<input class="form-control quantityInput" type="number" name="quantity" value="<?php echo $item_quantity ?>" >
 									</form>
 								</td>
 								<td><?php echo number_format($subtotal,2)?></td>
 								<td>
										<a href="../controllers/process_remove_item.php?id=<?php echo $indiv_item['id']?>" class="btn btn-success" >Remove Item</a>
									</td>
 							</tr>
 							<?php 
 						}
 					}
 					//2. get the session cart, and display each in a <tr>
	 					//a. check the data type of our $_SESSION['cart']
	 					//b. do the loop(foreach($data as key => $value))
	 					//c. the goal is to get the id and the quantity
	 					//d. get the item details through Item_query 
	 					//e. SELECT * FROM items WHERE id = the id we got from foreach.
	 					//f. use mysqli_query to get individual item.
	 					//g. get the subtotal by multiplying indiv_item ['price'] and quantity we got from the foreach.
		 					//get the total (in order fr us to do so, we must initializa $total at the very top)
		 					//display the details in <tr><td>

 					//REVIEW THIS PART LAYER
 					
 					//check if session exists
 					
 						//if session ['cart'] exists, loop through the array getting the name and the quantity

 					
 							//loop through our $items array to get the individual item information

 							
 								//check if $name (from $_SESSION['cart']) is equal to $indiv_item['name']

 							

 							
 					
 					 ?>
 					 <tr>
 					 	<td></td>
 					 	<td></td>
 					 	<td></td>
 					 	<td><a href="../controllers/process_empty_cart.php" class="btn btn-danger">Empty Cart</a></td>
 					 	<td>Total: <?php echo number_format($total,2) ?></td>
 					 </tr>
 					 <tr>
 					 	<td></td>
 					 	<td></td>
 					 	<td>
 					 		<form action="../controllers/process_checkout.php" method="POST">
 					 		<button class="btn-info" type="submit">Pay via COD</button>
 					 		</form>
 					 </td>
 					 	<td></td>
 					 	<td></td>
 					 	<td></td>
 					 </tr>
 					 
 					 
 					
 				</tbody>
 			</table>
 		
 	
 </div>
 <script type="text/javascript" src="../assets/scripts/updatecart.js"></script>
 <?php 
}
 ?>




 